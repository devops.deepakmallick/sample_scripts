#!/bin/bash
## This is use for printing the message
echo Hello world
## print multipule lines in single echo command

echo -e "Hello World line 1\nHello World line 2\nHello World line 3"

##\t ->tab

echo -e "NAME\tSUBJECT\nDeepak\tDevOps\nAryan\tMath"

##Print color for background & forground 

echo -e "\e[33mMY FORGROUND COLOR IS YELLOW"
echo -e "NO NEED FORGROUND COLOR"

##disable the color

echo -e "\e[31mMY FORGROUND COLOR IS RED\e[0m"
echo -e "NO NEED FORGROUND COLOR"

## Print both background & forground color

echo -e "\e[44;31mMY BGCOLOR IS YELLOW AND MY FGCOLOR IS RED\e[0m"
echo -e "\e[43;31mRed on yellow\e[0m"