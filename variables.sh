#!/bin/bash
## Declare  variables & Reading Variables

count=10
class=DevOps

echo -e "Welcome to $class class total strength is $count"
echo -e "CLASS NAME\t\tTOTAL STRENGTH\n$class\t\t$count"

## Local Variables
## Declare a variable on shell and try to access in script
## CLASS=DevOps

echo -e "MY CLASS NAME IS $CLASS"

##export CLASS=DevOps

echo -e "my class is $CLASS"

echo -e "Today date is 2021-10-24"
##########################################
DATE=$(date +%F)
echo -e "TODAY DATE IS $DATE"
############################################
ADD=$((2+3))

echo -e "ADDITION OF 2 AND 3 IS $ADD"

