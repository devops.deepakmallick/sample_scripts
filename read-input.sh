#!/bin/bash

## taking input from the user using read command and print the values in the screen

read -p 'Enter your name :' name
read -p 'Enter your age :' age
read -s -p 'Enter password :' pass

## Print the value in the screen

echo -e "MY NAME IS\t$name\nMY AGE IS\t$age\nMY PASSWORD IS\t$pass"